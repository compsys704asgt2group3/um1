import java.util.TimerTask;

public class DrawTimerTask extends TimerTask
{
	UM1 submarine = null;
	
	public DrawTimerTask(UM1 submarine)
    {
		this.submarine = submarine;
	}
	public void run()
    {
		submarine.repaint();
	}

}
