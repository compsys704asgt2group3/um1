import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Timer;

import java.util.*;
import java.io.*;
import java.net.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.JTextField;

import java.awt.event.*;

public class NetworkData
{
    private int RobotPort = 44000;
    private int LocalPort = 44400;
    private int OnePort = 44500;
    private int TwoPort = 44600;
    private int ThreePort = 44700;
    private int FourPort = 44222;
    private InetAddress OneAddress;
    private InetAddress TwoAddress;
    private InetAddress ThreeAddress;
    private InetAddress FourAddress;
    private InetAddress RobotAddress;

    NetworkData()
    {
        try
        {
            OneAddress = InetAddress.getByName("127.0.0.1");
            TwoAddress = InetAddress.getByName("127.0.0.1");
            ThreeAddress = InetAddress.getByName("127.0.0.1");
            FourAddress = InetAddress.getByName("127.0.0.1");
            RobotAddress = InetAddress.getByName("127.0.0.1");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public void SetRobotPort(int port)
    {
        this.RobotPort = port;
    }
    
    public void SetLocalPort(int port)
    {
        this.LocalPort = port;
    }
    
    public void SetOnePort(int port)
    {
        this.OnePort = port;
    }
    
    public void SetTwoPort(int port)
    {
        this.TwoPort = port;
    }
    
    public void SetThreePort(int port)
    {
        this.ThreePort = port;
    }
    
    public void SetFourPort(int port)
    {
        this.FourPort = port;
    }
    
    public void SetOneAddress(String adr)
    {
        try
        {
            this.OneAddress = InetAddress.getByName(adr);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public void SetTwoAddress(String adr)
    {
        try
        {
            this.TwoAddress = InetAddress.getByName(adr);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public void SetThreeAddress(String adr)
    {
        try
        {
            this.ThreeAddress = InetAddress.getByName(adr);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public void SetFourAddress(String adr)
    {
        try
        {
            this.FourAddress = InetAddress.getByName(adr);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public void SetRobotAddress(String adr)
    {
        try
        {
            this.RobotAddress = InetAddress.getByName(adr);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public int GetRobotPort()
    {
        return RobotPort;
    }
    
    public int GetLocalPort()
    {
        return LocalPort;
    }
    
    public int GetOnePort()
    {
        return OnePort;
    }
    
    public int GetTwoPort()
    {
        return TwoPort;
    }
    
    public int GetThreePort()
    {
        return ThreePort;
    }
    
    public int GetFourPort()
    {
        return FourPort;
    }
    
    public InetAddress GetOneAddress()
    {
        return OneAddress;
    }
    
    public InetAddress GetTwoAddress()
    {
        return TwoAddress;
    }
    
    public InetAddress GetThreeAddress()
    {
        return ThreeAddress;
    }
    
    public InetAddress GetFourAddress()
    {
        return FourAddress;
    }
    
    public InetAddress GetRobotAddress()
    {
        return RobotAddress;
    }
}