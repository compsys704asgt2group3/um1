package common;

// 2D point (x,y)
// Contains a serializer
public class Point //implements systemj.interfaces.Serializer
{
    public int x;
    public int y;
    
    public Point()
    {
    }
    
    public Point(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    
    public void SetX(int x)
    {
    	this.x = x;
    }
    
    public void SetY(int y)
    {
    	this.y = y;
    }
    
    public int GetX()
    {
    	return x;
    }
    
    public int GetY()
    {
    	return y;
    }
    
    public Object deserialize(byte[] b,int length)
    {
        // received "x,y"
        String stringToProcess = new String(b);
        int begin = 0;
        int end = stringToProcess.indexOf(",", begin);
        int x = Integer.parseInt(stringToProcess.substring(begin, end));
        begin = end + 1;
        int y = Integer.parseInt(stringToProcess.substring(begin));    
        return new Point(x,y);
    }
    
    public byte[] serialize(Object ob)
    {
        StringBuffer sb = new StringBuffer();
        sb.append("!@#$%^&*()"); // the header of the serialized data type
        sb.append(this.getClass().getName());
        sb.append("!@#$%^&*()"); // the footer of the serialized data type
        sb.append(x);
        sb.append(",");
        sb.append(y);
        return sb.toString().getBytes();
    }
}