import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

import java.util.zip.Adler32;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.imageio.ImageIO;

import java.lang.Thread;

import java.util.Random;
import java.util.Stack;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.geom.AffineTransform;

import java.nio.ByteBuffer;

import java.lang.Math;

import common.Point;


/*
http://www.mazeworks.com/mazegen/mazetut/index.htm

The maze is a 15x15 grid.

        rows    ---->
 
 cols
|
|
v

Each grid sqaure is an integer with the following bit properties:

                        Border      Walls  
0 0                 0 0 0 0     0 0 0 0
End Sensor      W S E N     W S E N

*/

//----------------------------------------------------------------------------------------
// Class to describe a Cell
//----------------------------------------------------------------------------------------
class Cell
{
    private int row; 
    private int col;
    
    public int GetRow()
    {
        return this.row;
    }

    public int GetCol()
    {
        return this.col;
    }
    
    public void SetCell(int row, int col)
    {
        this.row = row;
        this.col = col;
    }
    
    public Cell(int row, int col)
    {
        this.row = row;
        this.col = col;
    }
}

//----------------------------------------------------------------------------------------

// The submarine canvas. The maze is generated and displayed in this class.

//----------------------------------------------------------------------------------------
class UM1 extends JPanel
{
    // [rows][cols]
    public int maze[][] = new int[15][15];
    private byte mazeBytes[] = new byte[225];
    private byte mazeBytesOp[] = new byte[900];
    private Adler32 adler32 = new Adler32();
    private Adler32 adler32Op = new Adler32();
    
    public NetworkData networkData;
    public Boolean master = false;
    
    public Boolean handle1 = false;
    public Boolean handle2 = false;
    public Boolean handle3 = false;
    public Boolean handle4 = false;
    
    public BufferedImage background;
    public BufferedImage power_40x40;
    public BufferedImage triangle_40x40;
    public BufferedImage repair_40x40;
    public BufferedImage robotA_40x40;
    public BufferedImage robotB_40x40;
    public BufferedImage robotC_40x40;
    public BufferedImage robotD_40x40;
	public BufferedImage missionFailedImage;
	public BufferedImage greatSuccessImage;
    
    public static final int cGridPx = 600;
    public static final int cGridSize = 15;
    public static final int cCellPx = cGridPx / cGridSize;
    public static final int cTotalCells = cGridSize * cGridSize;
    
    public Boolean receivedMap = false;
    public char robot = 'A';
    
    public String robotsClose = "";
    
    // Enable debugging output
    // *******************************
    public static final Boolean debug = false;
    // *******************************
    
    public final static BasicStroke borderStroke = new BasicStroke(4.0f);
    public final static BasicStroke wallStroke = new BasicStroke(1.0f);
    
    public Color bluish = new Color(191,207,226);
    
    public Random randomGenerator = new Random();
    
    
    private Point A = new Point ( 20, 20 );
    private Point B = new Point ( 580, 20 );
    private Point C = new Point ( 20, 580 );
    private Point D = new Point ( 580, 580 );
	private String timeLeft = new String ( "10:00.0" );
	private int timeColour = 1;
	private int missionStatus = 1;

    
    //----------------------------------------------------------------------------------------
    public void SetRobot(char robot)
    {
        this.robot = robot;
    }
    
    //----------------------------------------------------------------------------------------
    public void UpdateA(Point a)
    {
        A = a;
        // if (master == true)
        // {
            RobotDetection();
        // }
    }
    
    //----------------------------------------------------------------------------------------
    public void UpdateB(Point b)
    {
        B = b;
        // if (master == true)
        // {
            RobotDetection();
        // }
    }
    
    //----------------------------------------------------------------------------------------
    public void UpdateC(Point c)
    {
        C = c;
        // if (master == true)
        // {
            RobotDetection();
        // }
    }
    
    //----------------------------------------------------------------------------------------
    public void UpdateD(Point d)
    {
        D = d;
        // if (master == true)
        // {
            RobotDetection();
        // }
    }
    
    
    //----------------------------------------------------------------------------------------
    public void UpdateHandle(Point pt)
    {
        switch (pt.GetX())
        {
        case 1:
            if (pt.GetY() == 0)
            {
                handle1 = false;
            }
            else
            {
                handle1 = true;
            }
            break;
        case 2:
            if (pt.GetY() == 0)
            {
                handle2 = false;
            }
            else
            {
                handle2 = true;
            }
            break;
        case 3:
            if (pt.GetY() == 0)
            {
                handle3 = false;
            }
            else
            {
                handle3 = true;
            }
            break;
        case 4:
            if (pt.GetY() == 0)
            {
                handle4 = false;
            }
            else
            {
                handle4 = true;
            }
            break;
        }
        if (handle1 == true && handle2 == true && handle3 == true && handle4 == true)
        {
            missionStatus = 2;
        }
    }

    //----------------------------------------------------------------------------------------
	public void UpdateTime(String timeLeftString, int colour, int s)
    { 
        timeLeft = timeLeftString; timeColour = colour;
        if (missionStatus != 2)
        {
            missionStatus = s;
        }
    }
    
    //----------------------------------------------------------------------------------------
    public synchronized void RobotDetection()
    {
        String data = "";
        switch (robot)
        {
        // Robot A Sensor
        case 'A':
            if (SensorOneRange(A, B) == true)
            {
                data += RobotLocationString(B);
            }
            if (SensorOneRange(A, C) == true)
            {
                data += RobotLocationString(C);
            }
            if (SensorOneRange(A, D) == true)
            {
                data += RobotLocationString(D);
            }
            if (data.isEmpty() == false)
            {
                robotsClose = data;
                SendString("127.0.0.1",44002, data);
            }
            else if (robotsClose.isEmpty() == false)
            {
                robotsClose = "";
                SendString("127.0.0.1",44002, "NULL");
            }
            data = "";
            break;
        // Robot B Sensor
        case 'B':
            if (SensorOneRange(A, B) == true)
            {
                data += RobotLocationString(B);
            }
            if (SensorOneRange(A, C) == true)
            {
                data += RobotLocationString(C);
            }
            if (SensorOneRange(A, D) == true)
            {
                data += RobotLocationString(D);
            }
            if (data.isEmpty() == false)
            {
                robotsClose = data;
                SendString("127.0.0.1",44102, data);
            }
            else if (robotsClose.isEmpty() == false)
            {
                robotsClose = "";
                SendString("127.0.0.1",44102, "NULL");
            }
            data = "";
            break;
        
        case 'C':
            // Robot C Sensor
            if (SensorOneRange(C, A) == true)
            {
                data += RobotLocationString(A);
            }
            if (SensorOneRange(C, B) == true)
            {
                data += RobotLocationString(B);
            }
            if (SensorOneRange(C, D) == true)
            {
                data += RobotLocationString(D);
            }
            if (data.isEmpty() == false)
            {
                robotsClose = data;
                SendString("127.0.0.1",44202, data);
            }
            else if (robotsClose.isEmpty() == false)
            {
                robotsClose = "";
                SendString("127.0.0.1",44202, "NULL");
            }
            data = "";
            break;
        
        case 'D':
            // Robot D Sensor
            if (SensorOneRange(D, A) == true)
            {
                data += RobotLocationString(A);
            }
            if (SensorOneRange(D, B) == true)
            {
                data += RobotLocationString(B);
            }
            if (SensorOneRange(D, C) == true)
            {
                data += RobotLocationString(C);
            }
            if (data.isEmpty() == false)
            {
                robotsClose = data;
                SendString("127.0.0.1",44302, data);
            }
            else if (robotsClose.isEmpty() == false)
            {
                robotsClose = "";
                SendString("127.0.0.1",44302, "NULL");
            }
            data = "";
            break;
        }
    }
    
    //----------------------------------------------------------------------------------------
    public void SendString(String ip, int port, String data)
    {
        try
        {
            InetAddress IPAddress = InetAddress.getByName("127.0.0.1");
            DatagramSocket clientSocket = new DatagramSocket();
            byte[] sendData = data.getBytes();
            System.out.println("SendRobotSensor(UDP)->Robot: " + data);
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
            clientSocket.send(sendPacket);
            clientSocket.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    //----------------------------------------------------------------------------------------
    public String RobotLocationString(Point A)
    {
        String data = "";
        data = A.GetX() + "," + A.GetY() + ";";
        return data;
    }
    
    //----------------------------------------------------------------------------------------
    public Boolean SensorOneRange(Point A, Point B)
    {
        if ( (Math.abs(B.GetX() - A.GetX()) < 24)  && (Math.abs(B.GetY() - A.GetY()) < 24) )
        {
            return true;
        }
        return false;
    }
    
    //----------------------------------------------------------------------------------------
    UM1()
    {
        this.repaint();
        try
        {
            background = ImageIO.read(new File("maze_background_600x600.png"));
            power_40x40 = ImageIO.read(new File("power_40x40.png"));
            triangle_40x40 = ImageIO.read(new File("motion_40x40.png"));
            repair_40x40 = ImageIO.read(new File("repair_40x40.png"));
            robotA_40x40 = ImageIO.read(new File("robot_20x20.png"));
            robotB_40x40 = ImageIO.read(new File("robot2_20x20.png"));
            robotC_40x40 = ImageIO.read(new File("robot3_20x20.png"));
            robotD_40x40 = ImageIO.read(new File("robot4_20x20.png"));
			missionFailedImage = ImageIO.read(new File("missionFailed.png"));
			greatSuccessImage = ImageIO.read(new File("greatSuccess.png"));
        }
        catch (IOException ex)
        {
            System.out.println("Could not read maze_background_600x600.png Exiting.");
        }
    }
    
    //----------------------------------------------------------------------------------------
    public void SetNetworkData(NetworkData networkData)
    {
        this.networkData = networkData;
    }
    
    //----------------------------------------------------------------------------------------
    public void SetMap(byte[] data, int dataLength)
    {
        if (true)
        {
            receivedMap = true;
            for (int x = 0; x < 900; ++x)
            {
                this.mazeBytesOp[x] = data[x];
            }
            int n = 0;
            for (int row = 0; row < 15; ++row)
            {
                for (int col = 0; col < 15; ++col)
                {
                    maze[row][col] = 0;
                    maze[row][col] = maze[row][col] | ((mazeBytesOp[n] & 0xFF) << 24);
                    ++n;
                    maze[row][col] = maze[row][col] | ((mazeBytesOp[n] & 0xFF) << 16);
                    ++n;
                    maze[row][col] = maze[row][col] | ((mazeBytesOp[n] & 0xFF) << 8);
                    ++n;
                    maze[row][col] = maze[row][col] | ((mazeBytesOp[n] & 0xFF) << 0);
                    ++n;
                }
            }
            GenerateMazeByteArray();
            System.out.println("Received map!");
        }
        else
        {
            System.out.println("ERROR SetMap: data.length < 900");
        }
    }
    
    //----------------------------------------------------------------------------------------
    public void GenerateMaze()
    {
        receivedMap = true;
        InitMaze();
        CreateBorders();
        CreateWalls();
        CreateInnerArea();
        SpawnPowerPoints();
        SpawnMotionSensors();
        GenerateMazeByteArray();
        GenerateMazeByteArrayOp();
        this.repaint();
        Debug();
    }
    
    //----------------------------------------------------------------------------------------
    public void InitMaze()
    {
        for (int i = 0; i < cGridSize; ++i)
        {
            for (int j = 0; j < cGridSize; ++j)
            {
                maze[i][j] = 0x0000000F;
            }
        }
    }
    
    //----------------------------------------------------------------------------------------
    public void GenerateMazeByteArrayOp()
    {
        int n = 0;
        byte tmpByte = 0x00;
        for (int row = 0; row < 15; ++row)
        {
            for (int col = 0; col < 15; ++col)
            {
                mazeBytesOp[n] = (byte)((maze[row][col] >>> 24) & 0xFF);
                ++n;
                mazeBytesOp[n] = (byte)((maze[row][col] >>> 16) & 0xFF);
                ++n;
                mazeBytesOp[n] = (byte)((maze[row][col] >>> 8) & 0xFF);
                ++n;
                mazeBytesOp[n] = (byte)((maze[row][col] >>> 0) & 0xFF);
                ++n;
            }
        }
        adler32Op.reset();
        adler32Op.update(mazeBytes);
        System.out.println("adler32->Operators: " + (int)adler32Op.getValue());
        SendMazeByteArrayOp();
    }
    
    //----------------------------------------------------------------------------------------
    public void GenerateMazeByteArray()
    {
        int n = 0;
        int tmpInt = 0x00000000;
        for (int row = 0; row < 15; ++row)
        {
            for (int col = 0; col < 15; ++col)
            {
                tmpInt = 0x00000000;
                // North
                if ((int)(maze[row][col] & 0x00000011) > 0)
                {
                    tmpInt = tmpInt | 0x00000001;
                }
                // East
                if ((int)(maze[row][col] & 0x00000022) > 0)
                {
                    tmpInt = tmpInt | 0x00000002;
                }
                // South
                if ((int)(maze[row][col] & 0x00000044) > 0)
                {
                    tmpInt = tmpInt | 0x00000004;
                }
                // West
                if ((int)(maze[row][col] & 0x00000088) > 0)
                {
                    tmpInt = tmpInt | 0x00000008;
                }
                // Sensor
                if ((int)(maze[row][col] & 0x00000100) > 0)
                {
                    tmpInt = tmpInt | 0x00000010;
                }
                // End
                if ((int)(maze[row][col] & 0x00000200) > 0)
                {
                    tmpInt = tmpInt | 0x00000020;
                }
                mazeBytes[n] = (byte)tmpInt;
                ++n;
            }
        }
        adler32.reset();
        adler32.update(mazeBytes);
        
        System.out.println("adler32->Robot: " + (int)adler32.getValue());
        SendMazeByteArray(robot);
    }
    
    //----------------------------------------------------------------------------------------
    public void SendMazeByteArrayOp()
    {
        try
        {
            DatagramSocket clientSocket = new DatagramSocket();
            System.out.println("SendMazeByteArrayOp(UDP)->Operators: 900 bytes");
            DatagramPacket sendPacket = new DatagramPacket(mazeBytesOp, mazeBytesOp.length, networkData.GetOneAddress(), networkData.GetOnePort() + 5);
            clientSocket.send(sendPacket);
            sendPacket = new DatagramPacket(mazeBytesOp, mazeBytesOp.length, networkData.GetTwoAddress(), networkData.GetTwoPort() + 5);
            clientSocket.send(sendPacket);
            sendPacket = new DatagramPacket(mazeBytesOp, mazeBytesOp.length, networkData.GetThreeAddress(), networkData.GetThreePort() + 5);
            clientSocket.send(sendPacket);
            clientSocket.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    //----------------------------------------------------------------------------------------
    public void Debug()
    {
        int n = 0;
        for (int row = 0; row < 15; ++row)
        {
            for (int col = 0; col < 15; ++col)
            {
                maze[row][col] = 0;
                maze[row][col] = maze[row][col] | ((mazeBytesOp[n] & 0xFF) << 24);
                ++n;
                maze[row][col] = maze[row][col] | ((mazeBytesOp[n] & 0xFF) << 16);
                ++n;
                maze[row][col] = maze[row][col] | ((mazeBytesOp[n] & 0xFF) << 8);
                ++n;
                maze[row][col] = maze[row][col] | ((mazeBytesOp[n] & 0xFF) << 0);
                ++n;
            }
        }
        this.repaint();
    }
    
    //----------------------------------------------------------------------------------------
    public void SendMazeByteArray(char robot)
    {
        try
        {
            InetAddress IPAddress = InetAddress.getByName("127.0.0.1");;
            int port = 44100;
            DatagramSocket clientSocket = new DatagramSocket();
            System.out.println("SendMazeByteArray(UDP)->robot: 225 bytes");
            DatagramPacket sendPacket = new DatagramPacket(mazeBytes, mazeBytes.length, networkData.GetRobotAddress(), networkData.GetRobotPort() + 1);
            clientSocket.send(sendPacket);
            clientSocket.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    //----------------------------------------------------------------------------------------
    public void CreateBorders()
    {
        // 4 outer corners
        maze[0][0] = maze[0][0] | 0x00000090;
        maze[14][0] = maze[14][0] | 0x000000C0;
        maze[0][14] = maze[0][14] | 0x00000030;
        maze[14][14] = maze[14][14] | 0x00000060;
        
        // four outer walls
        for (int i = 1; i < 14; ++i)
        {
            maze[0][i] = maze[0][i] | 0x00000010;
            maze[i][14] = maze[i][14] | 0x00000020;
            maze[14][i] = maze[14][i] | 0x00000040;
            maze[i][0] = maze[i][0] | 0x00000080;
        }
        
        // inner walls
        maze[5][6] = maze[5][6] | 0x00000040;
        maze[5][7] = maze[5][7] | 0x00000040;
        maze[5][8] = maze[5][8] | 0x00000040;
        maze[6][5] = maze[6][5] | 0x00000020;
        maze[6][6] = maze[6][6] | 0x00000090;
        maze[6][7] = maze[6][7] | 0x00000010;
        maze[6][8] = maze[6][8] | 0x00000030;
        maze[6][9] = maze[6][9] | 0x00000080;
        maze[8][5] = maze[8][5] | 0x00000020;
        maze[8][6] = maze[8][6] | 0x000000C0;
        maze[8][7] = maze[8][7] | 0x00000040;
        maze[8][8] = maze[8][8] | 0x00000060;
        maze[8][9] = maze[8][9] | 0x00000080;
        maze[9][6] = maze[9][6] | 0x00000010;
        maze[9][7] = maze[9][7] | 0x00000010;
        maze[9][8] = maze[9][8] | 0x00000010;
        
        // Temporary walls to prevent the algorithm from going inside the middle area
        maze[7][5] = maze[7][5] | 0x00000020;
        maze[7][6] = maze[7][6] | 0x00000080;
        maze[7][8] = maze[7][8] | 0x00000020;
        maze[7][9] = maze[7][9] | 0x00000080;
    }
    
    //----------------------------------------------------------------------------------------
    public void SpawnPowerPoints()
    {
        maze[6][6] = maze[6][6] | 0x00000200;
        maze[8][6] = maze[8][6] | 0x00000200;
        maze[6][8] = maze[6][8] | 0x00000200;
        maze[8][8] = maze[8][8] | 0x00000200;
    }
    
    //----------------------------------------------------------------------------------------
    public void SpawnMotionSensors()
    {
        int sensorsSpawned = 0;
        int row = 0;
        int col = 0;
        while (true)
        {
            if (sensorsSpawned == 20) { break; }
            while (true)
            {
                col  = randomGenerator.nextInt(15);
                row = randomGenerator.nextInt(15);
                if ( IsSensorSpawnValid(row, col) == true)
                    break;
                else
                    continue;
            }
            maze[row][col] = maze[row][col] | 0x00000100;
            ++sensorsSpawned;
        }
    }
    
    //----------------------------------------------------------------------------------------
    public Boolean IsSensorSpawnValid(int row, int col)
    {
            // 4 corners
            if (row == 0 && col == 0)
                return false;
            if (row == 0 && col == 14)
                return false;
            if (row == 14 && col == 0)
                return false;
            if (row == 14 && col == 14)
                return false;
            
            // Inner area
            if (row == 6 && col == 6)
                return false;
            if (row == 6 && col == 7)
                return false;
            if (row == 6 && col == 8)
                return false;
            if (row == 7 && col == 6)
                return false;
            if (row == 7 && col == 7)
                return false;
            if (row == 7 && col == 8)
                return false;
            if (row == 8 && col == 6)
                return false;
            if (row == 8 && col == 7)
                return false;
            if (row == 8 && col == 8)
                return false;
            return true;
    }
    
    //----------------------------------------------------------------------------------------
    public void SetMaster(Boolean value)
    {
        this.master = value;
    }
    
    //----------------------------------------------------------------------------------------
    public void CreateInnerArea()
    {
        for (int row = 6; row < 9; ++row)
        {
            for (int col = 6; col < 9; ++col)
            {
                maze[row][col] = maze[row][col] & 0xFFFFFF0;
            }
        }
        maze[7][5] = maze[7][5] & 0xFFFFFFFD;
        maze[7][9] = maze[7][9] & 0xFFFFFFF7;
        
        maze[7][5] = maze[7][5] & 0xFFFFFF0F;
        maze[7][6] = maze[7][6] & 0xFFFFFF0F;
        maze[7][8] = maze[7][8] & 0xFFFFFF0F;
        maze[7][9] = maze[7][9] & 0xFFFFFF0F;
    }
    
    //----------------------------------------------------------------------------------------
    public void CreateWalls()
    {
        Stack<Cell> CellStack = new Stack<Cell>();
        Cell currentCell = initCurrentCell();
        Cell neighbourCell = new Cell(0,0);
        int visitedCells = 1;
        ArrayList<Integer> neighbours = new ArrayList<Integer>();
        int randomNeighbour = 0;
        int temp = 0;
        if (debug) { System.out.println("Initial cell: " + currentCell.GetRow() + " " +currentCell.GetCol()); }
        while(visitedCells < cTotalCells - 9)
        {
            ++temp;
            //if (temp == 200) { return; }
            if (currentCell.GetRow() > 5 && currentCell.GetRow() < 9 && currentCell.GetCol() > 5 && currentCell.GetCol()  < 9 && debug) { System.out.println("BREAK: inside middle square "); return; }
            //if (temp > 500) { return; }
            if (debug) { System.out.println("visited Cells: " + visitedCells); }
            if (debug) { System.out.println("currentCell: " + currentCell.GetRow() + " " +currentCell.GetCol()); }
            neighbours.clear();
            //  find all neighbors of CurrentCell with all walls intact
            // west
            if (currentCell.GetCol() != 0)
            {
                if ( (IsWestBorder(currentCell.GetRow(), currentCell.GetCol()) == false) && (WallsIntact(currentCell.GetRow(), currentCell.GetCol() - 1) == true) )
                {
                    neighbours.add(3);
                }
            }
            // east
            if (currentCell.GetCol() != 14)
            {
                if (  (IsEastBorder(currentCell.GetRow(), currentCell.GetCol()) == false) && (WallsIntact(currentCell.GetRow(), currentCell.GetCol() + 1) == true) )
                {
                    neighbours.add(1);
                }
            }
            // north
            if (currentCell.GetRow() != 0)
            {
                if ( (IsNorthBorder(currentCell.GetRow(), currentCell.GetCol()) == false) && (WallsIntact(currentCell.GetRow() - 1, currentCell.GetCol()) == true) )
                {
                    neighbours.add(0);
                }
            }
            // south
            if (currentCell.GetRow() != 14)
            {
                if ( (IsSouthBorder(currentCell.GetRow(), currentCell.GetCol()) == false) && (WallsIntact(currentCell.GetRow() + 1, currentCell.GetCol()) == true) )
                {
                    neighbours.add(2);
                }
            }
             if (debug) { System.out.println("neighbours size: " + neighbours.size()); }
            if (neighbours.size() > 0)
            {
                randomNeighbour = randomGenerator.nextInt(neighbours.size());
                randomNeighbour = neighbours.get(randomNeighbour);
                if (debug) { System.out.println("randomNeighbour: " + randomNeighbour); }
                switch (randomNeighbour)
                {
                // North
                case 0:
                    neighbourCell.SetCell( currentCell.GetRow() - 1, currentCell.GetCol() );
                    break;
                // East
                case 1:
                    neighbourCell.SetCell( currentCell.GetRow(), currentCell.GetCol() + 1 );
                    break;
                // South
                case 2:
                    neighbourCell.SetCell( currentCell.GetRow() + 1, currentCell.GetCol() );
                    break;
                // West
                case 3:
                    neighbourCell.SetCell( currentCell.GetRow(), currentCell.GetCol() - 1 );
                    break;
                }
                KnockWall( currentCell, randomNeighbour );
                CellStack.push( new Cell( currentCell.GetRow(), currentCell.GetCol()) );
                currentCell.SetCell( neighbourCell.GetRow(), neighbourCell.GetCol() );
                ++visitedCells;
            }
            else
            {
                currentCell = CellStack.pop();
            }
        }
    }
    

    //----------------------------------------------------------------------------------------
    public void paint(Graphics g)
    {
	
        if (receivedMap == true)
        {	
            super.paintComponent(g);
            Graphics2D g2D = (Graphics2D) g;
			g2D.setColor(Color.BLACK);
			g2D.drawImage(background,null, 0, 0);
			g2D.drawImage(power_40x40,null, 240, 240);
            g2D.drawImage(power_40x40,null, 240, 320);
            g2D.drawImage(power_40x40,null, 320, 240);
            g2D.drawImage(power_40x40,null, 320, 320);
            DrawMap(g2D);
            g2D.drawImage(robotA_40x40,null, A.x - 10, A.y - 10);
            g2D.drawImage(robotB_40x40,null, B.x - 10, B.y - 10);
            g2D.drawImage(robotC_40x40,null, C.x - 10, C.y - 10);
            g2D.drawImage(robotD_40x40,null, D.x - 10, D.y - 10);
            if (handle1 == true)
                g2D.drawImage(repair_40x40,null, 240, 240);
            if (handle2 == true)
                g2D.drawImage(repair_40x40,null, 320, 240);
            if (handle3 == true)
                g2D.drawImage(repair_40x40,null, 320, 320);
            if (handle4 == true)
                g2D.drawImage(repair_40x40,null, 240, 320);
			DrawTime(g2D);

        }
        else
        {
            super.paintComponent(g);
            Graphics2D g2D = (Graphics2D) g;
        }
    }
    
    //----------------------------------------------------------------------------------------
    public void DrawMap(Graphics2D g2D)
    {
        g2D.setColor(Color.BLACK);
        
        for (int row = 0; row < cGridSize; ++row)
        {
            for (int col = 0; col < cGridSize; ++col)
            {
                if (IsMotionSensor(row, col) == true)
                    g2D.drawImage(triangle_40x40, null, col * 40, row * 40);
                DrawBox(g2D, row, col);
            }
        }
    }
	
    //----------------------------------------------------------------------------------------
    public void DrawTime(Graphics2D g2D)
    {
		Font font = new Font("Arial", Font.BOLD, 30);
		g2D.setFont(font);
		
		switch (timeColour)
		{
			case 1: g2D.setColor(Color.GREEN); break;
			case 2: g2D.setColor(new Color(128, 255, 128)); break;
			case 3: g2D.setColor(Color.ORANGE); break;
			case 4: g2D.setColor(new Color(255, 128, 128)); break;
			case 5: g2D.setColor(Color.RED); break;
			default: g2D.setColor(Color.GREEN); break;
		}
		
		if (missionStatus == 2)
        {
			 g2D.drawImage(greatSuccessImage, null, 0, 0);
		} 
        else if (missionStatus == 1)
        {
			g2D.drawString(timeLeft, 250, 310);
		}
        else
        {
            g2D.drawImage(missionFailedImage, null, 0, 0);
        }
	}
            
    
    //----------------------------------------------------------------------------------------
    public Cell initCurrentCell()
    {
        int row = 0;
        int col = 0;
        
        while (true)
        {
            row = randomGenerator.nextInt(15);
            if ( row < 6 ||  row > 8 )
                break;
        }
        while (true)
        {
            col = randomGenerator.nextInt(15);
            if ( col < 6 ||  row > 8 )
                break;
        }
        return new Cell(row,col);
    }
    
    //----------------------------------------------------------------------------------------
    public Boolean WallsIntact( int row, int col )
    {
        if (debug) { System.out.println("WallsIntact.row.col: " + row+ " " + col); }
        if (debug) { System.out.println("WallsIntact.return: " + ((int)(maze[row][col] & 0x0000000F) == 15) + " " + maze[row][col]); }
        return (int)(maze[row][col] & 0x0000000F) == 15;
    }
    
    //----------------------------------------------------------------------------------------
    public void KnockWall( Cell currentCell, int direction )
    {
        int currentCellRow = currentCell.GetRow();
        int currentCellCol = currentCell.GetCol();
        if (debug) { System.out.println("KnockWall.currentCell.direction: " + currentCellRow + " " +currentCellCol + " " + direction); }
        switch (direction)
        {
        // North
        case 0:
            maze[currentCellRow][currentCellCol] = (int)(maze[currentCellRow][currentCellCol] & 0xFFFFFFFE);
            maze[currentCellRow - 1][currentCellCol] = (int)(maze[currentCellRow - 1][currentCellCol] & 0xFFFFFFFB);
            break;
        // East
        case 1:
            maze[currentCellRow][currentCellCol] = (int)(maze[currentCellRow][currentCellCol] & 0xFFFFFFFD);
            maze[currentCellRow][currentCellCol + 1] = (int)(maze[currentCellRow][currentCellCol + 1] & 0xFFFFFFF7);
            break;
        // South
        case 2:
            maze[currentCellRow][currentCellCol] = (int)(maze[currentCellRow][currentCellCol] & 0xFFFFFFFB);
            maze[currentCellRow + 1][currentCellCol] = (int)(maze[currentCellRow + 1][currentCellCol] & 0xFFFFFFFE);
            break;
        // West
        case 3:
            maze[currentCellRow][currentCellCol] = (int)(maze[currentCellRow][currentCellCol] & 0xFFFFFFF7);
            maze[currentCellRow][currentCellCol - 1] = (int)(maze[currentCellRow][currentCellCol - 1] & 0xFFFFFFFD);
            break;
        }
    }

    //----------------------------------------------------------------------------------------
    public Boolean IsMotionSensor(int row, int col)
    {
        return (int)(maze[row][col] & 0x00000100) > 0;
    }
    
    //----------------------------------------------------------------------------------------
    public Boolean IsWestWall(int row, int col)
    {
        return (int)(maze[row][col] & 0x00000008) > 0;
    }
    
    //----------------------------------------------------------------------------------------
    public Boolean IsEastWall(int row, int col)
    {
        return (int)(maze[row][col] & 0x00000002) > 0;
    }
    
    //----------------------------------------------------------------------------------------
    public Boolean IsSouthWall(int row, int col)
    {
        return (int)(maze[row][col] & 0x00000004) > 0;
    }
    
    //----------------------------------------------------------------------------------------
    public Boolean IsNorthWall(int row, int col)
    {
        return (int)(maze[row][col] & 0x00000001) > 0;
    }
    
    //----------------------------------------------------------------------------------------
    public Boolean IsWestBorder(int row, int col)
    {
        return (int)(maze[row][col] & 0x00000080) > 0;
    }
    
    //----------------------------------------------------------------------------------------
    public Boolean IsEastBorder(int row, int col)
    {
        return (int)(maze[row][col] & 0x00000020) > 0;
    }
    
    //----------------------------------------------------------------------------------------
    public Boolean IsSouthBorder(int row, int col)
    {
        return (int)(maze[row][col] & 0x00000040) > 0;
    }
    
    //----------------------------------------------------------------------------------------
    public Boolean IsNorthBorder(int row, int col)
    {
        return (int)(maze[row][col] & 0x00000010) > 0;
    }
    
    //----------------------------------------------------------------------------------------
    public void DrawBox(Graphics2D g2D, int row, int col)
    {
        g2D.setColor(Color.RED);
        
        // draw north border
        if (IsNorthBorder(row, col))
        {
            g2D.setStroke(borderStroke);
            g2D.drawLine( (col + 1) * cCellPx - cCellPx, (row + 1) * cCellPx - cCellPx, (col + 1) * cCellPx, (row + 1) * cCellPx - cCellPx );
        }
        else if (IsNorthWall(row,col))
        {
            g2D.setStroke(wallStroke);
            g2D.drawLine( (col + 1) * cCellPx - cCellPx, (row + 1) * cCellPx - cCellPx, (col + 1) * cCellPx, (row + 1) * cCellPx - cCellPx );
        }
        
       // draw south border
        if (IsSouthBorder(row, col))
        {
            g2D.setStroke(borderStroke);
            g2D.drawLine( (col + 1) * cCellPx - cCellPx, (row + 1) * cCellPx, (col + 1) * cCellPx, (row + 1) * cCellPx );
        }
        else if (IsSouthWall(row,col))
        {
            g2D.setStroke(wallStroke);
            g2D.drawLine( (col + 1) * cCellPx - cCellPx, (row + 1) * cCellPx, (col + 1) * cCellPx, (row + 1) * cCellPx );
        }
            
        // draw east border
        if (IsEastBorder(row,col))
        {
            g2D.setStroke(borderStroke);
            g2D.drawLine( (col + 1) * cCellPx, (row + 1) * cCellPx - cCellPx, (col + 1) * cCellPx, (row + 1) * cCellPx );
        }
        else if (IsEastWall(row,col))
        {
            g2D.setStroke(wallStroke);
            g2D.drawLine( (col + 1) * cCellPx, (row + 1) * cCellPx - cCellPx, (col + 1) * cCellPx, (row + 1) * cCellPx );
        }
            
        // draw west border
        if (IsWestBorder(row,col))
        {
            g2D.setStroke(borderStroke);
            g2D.drawLine( (col + 1) * cCellPx - cCellPx, (row + 1) * cCellPx - cCellPx, (col + 1) * cCellPx - cCellPx, (row + 1) * cCellPx );
        }
        else if (IsWestWall(row,col))
        {
            g2D.setStroke(wallStroke);
            g2D.drawLine( (col + 1) * cCellPx - cCellPx, (row + 1) * cCellPx - cCellPx, (col + 1) * cCellPx - cCellPx, (row + 1) * cCellPx );
        }
        g2D.setStroke(new BasicStroke());
    }
}
    