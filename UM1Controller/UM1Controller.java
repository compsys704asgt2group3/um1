import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Timer;

import java.util.*;
import java.io.*;
import java.net.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.JTextField;

import java.awt.event.*;

//----------------------------------------------------------------------------------------
// The GUI controller. This Controller will operate 1 robot on the submarine.
// The UM1 class represents the submarine (the canvas).
//----------------------------------------------------------------------------------------
public class UM1Controller extends JFrame implements ActionListener, MouseListener, KeyListener
{
    static final String NEWLINE = System.getProperty("line.separator");
    
    // **********************************
    static final Boolean debug_keyboard = false;
    // **********************************
    
    private char robot = 'A';
    private Boolean master = false;
    private Timer t1 = new Timer();
    private 	DrawTimerTask tt;
    
    private JButton GenerateMazeButton = new JButton("Generate Maze");
	private JButton StartButton = new JButton("Start Timer");
    private JTextField RobotNameTextField = new JTextField("A");
    private JTextField RobotIPTextField = new JTextField("127.0.0.1:44000");
    private JTextField LocalPortTextField = new JTextField("44400");
    private JTextField OneIPTextField = new JTextField("127.0.0.1:44500");
    private JTextField TwoIPTextField = new JTextField("127.0.0.1:44600");
    private JTextField ThreeIPTextField = new JTextField("127.0.0.1:44700");
    private JTextField FourIPTextField = new JTextField("127.0.0.1:44800");
    
    private RxRobotLocationThread robotRx;
    private RxBroadcastLocationThread broadcast1Rx;
    private RxBroadcastLocationThread broadcast2Rx;
    private RxBroadcastLocationThread broadcast3Rx;
    private RxBroadcastLocationThread broadcast4Rx;
    private RxHandleThread handleRx;
    private RxBroadcastHandleThread broadcastHandle1Rx;
    private RxBroadcastHandleThread broadcastHandle2Rx;
    private RxBroadcastHandleThread broadcastHandle3Rx;
    private RxBroadcastHandleThread broadcastHandle4Rx;
    private RxMapThread mapRx;
	private TimerThread timerThread;
    
    private JButton MasterButton = new JButton("Master");
    private JButton MirrorButton = new JButton("NOT Master");
    
    // Set of currently pressed keys
    private final Set<Character> pressed = new HashSet<Character>();
    
    // The submarine canvas
    public UM1 submarine;
    // Network info
    public NetworkData networkData = new NetworkData();
    
    //----------------------------------------------------------------------------------------
    public static void main(String arg[])
    {
        new UM1Controller();
    }
    
    //----------------------------------------------------------------------------------------
    public UM1Controller()
    {
        this.setBackground(Color.white);
        setTitle("UM1 Controller");
        setSize(700, 700);
        setLocation(500, 50);
        setPreferredSize(new Dimension(700,700));
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        
        Container container = getContentPane();
        container.setLayout(new BorderLayout());
        container.setBackground(Color.white);
        
        addWindowListener(
            new WindowAdapter()
            {
                public void windowClosing(WindowEvent e)
                {
                  System.exit(0);
                }
            }
        );
        JPanel panel = getUM1Panel();
        JScrollPane scroller = new JScrollPane(panel);
        container.add(scroller);
        
        submarine.addKeyListener(this);
        submarine.addMouseListener(this);
        
        
        submarine.SetNetworkData(networkData);
        
        System.out.println("Focusable: " + submarine.isFocusable());
        
        tt = new DrawTimerTask(submarine);
        t1.schedule(tt, 0, 17);

        robotRx = new RxRobotLocationThread(submarine, networkData, 44400, 'A');
        robotRx.start();
        mapRx = new RxMapThread(submarine, networkData, 44405, 'A');
        mapRx.start();
		timerThread = new TimerThread(submarine, networkData, 44407);
        timerThread.start();
        broadcast1Rx = new RxBroadcastLocationThread(submarine, networkData, 44401, 'A');
        broadcast2Rx = new RxBroadcastLocationThread(submarine, networkData, 44402, 'B');
        broadcast3Rx = new RxBroadcastLocationThread(submarine, networkData, 44403, 'C');
        broadcast4Rx = new RxBroadcastLocationThread(submarine, networkData, 44404, 'D');
        handleRx = new RxHandleThread(submarine, networkData, 44408, 'A');
        broadcastHandle1Rx = new RxBroadcastHandleThread(submarine, networkData, 44409, 'A');
        broadcastHandle2Rx = new RxBroadcastHandleThread(submarine, networkData, 44410, 'B');
        broadcastHandle3Rx = new RxBroadcastHandleThread(submarine, networkData, 44411, 'C');
        broadcastHandle4Rx = new RxBroadcastHandleThread(submarine, networkData, 44412, 'D');
        broadcast1Rx.SetRobot('A');
        broadcast2Rx.SetRobot('B');
        broadcast3Rx.SetRobot('C');
        broadcast4Rx.SetRobot('D');
        broadcastHandle1Rx.SetRobot('A');
        broadcastHandle2Rx.SetRobot('B');
        broadcastHandle3Rx.SetRobot('C');
        broadcastHandle4Rx.SetRobot('D');
        broadcast1Rx.start();
        broadcast2Rx.start();
        broadcast3Rx.start();
        broadcast4Rx.start();
        broadcastHandle1Rx.start();
        broadcastHandle2Rx.start();
        broadcastHandle3Rx.start();
        broadcastHandle4Rx.start();
        handleRx.start();
        
        setResizable(true);
    	setVisible(true);
        submarine.repaint();
    }
    
    //----------------------------------------------------------------------------------------
    private JPanel getUM1Panel()
    {
        JPanel p = new JPanel(new GridBagLayout());
        submarine = new UM1();
        p.setBackground(Color.black);
        GridBagConstraints c = new GridBagConstraints();

        c.gridx = 0;
        c.gridy = 0;
        c.gridheight = 1;
        c.gridwidth = 600;
        c.ipadx = 590;
        c.ipady = 590;
        c.weightx = 0;
        c.weighty = 0;
        p.add(submarine, c);
        
        GenerateMazeButton.setActionCommand("GENERATEMAZE_PRESS");
        GenerateMazeButton.addActionListener(this);
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 3;
        c.ipadx = 100;
        c.ipady = 1;
        c.weightx = 0.1;
        c.weighty = 0.1;
        p.add(GenerateMazeButton, c);
		
		StartButton.setActionCommand("STARTTIMER_PRESS");
        StartButton.addActionListener(this);
        c.gridwidth = 1;
        c.gridx = 1;
        c.gridy = 3;
        c.ipadx = 100;
        c.ipady = 1;
        c.weightx = 0.1;
        c.weighty = 0.1;
        p.add(StartButton, c);
        
        MasterButton.setActionCommand("MASTER_PRESS");
        MasterButton.addActionListener(this);
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 4;
        c.ipadx = 100;
        c.ipady = 1;
        c.weightx = 0.1;
        c.weighty = 0.1;
        p.add(MasterButton, c);
        
        MirrorButton.setActionCommand("MIRROR_PRESS");
        MirrorButton.addActionListener(this);
        c.gridwidth = 1;
        c.gridx = 1;
        c.gridy = 4;
        c.ipadx = 100;
        c.ipady = 1;
        c.weightx = 0.1;
        c.weighty = 0.1;
        p.add(MirrorButton, c);

        JLabel LocalPortLabel = new JLabel("Local Port");
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 5;
        c.ipadx = 74;
        c.ipady = 1;
        c.weightx = 0.1;
        c.weighty = 0.1;
        p.add(LocalPortLabel,c);
        
        LocalPortTextField.setEditable(true);
        c.gridwidth = 1;
        c.gridx = 1;
        c.gridy = 5;
        c.ipadx = 100;
        c.ipady = 1;
        c.weightx = 0.1;
        c.weighty = 0.1;
        p.add(LocalPortTextField,c);
        
        JLabel RobotNameLabel = new JLabel("Robot Name");
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 6;
        c.ipadx = 74;
        c.ipady = 1;
        c.weightx = 0.1;
        c.weighty = 0.1;
        p.add(RobotNameLabel,c);
        
        RobotNameTextField.setEditable(true);
        c.gridwidth = 1;
        c.gridx = 1;
        c.gridy = 6;
        c.ipadx = 100;
        c.ipady = 1;
        c.weightx = 0.1;
        c.weighty = 0.1;
        p.add(RobotNameTextField,c);
        
        JLabel RobotLabel = new JLabel("Robot Address");
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 7;
        c.ipadx = 74;
        c.ipady = 1;
        c.weightx = 0.1;
        c.weighty = 0.1;
        p.add(RobotLabel,c);
        
        RobotIPTextField.setEditable(true);
        c.gridwidth = 1;
        c.gridx = 1;
        c.gridy = 7;
        c.ipadx = 100;
        c.ipady = 1;
        c.weightx = 0.1;
        c.weighty = 0.1;
        p.add(RobotIPTextField,c);
        
        JLabel ALabel = new JLabel("Operator #1");
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 8;
        c.ipadx = 100;
        c.ipady = 1;
        c.weightx = 0.1;
        c.weighty = 0.1;
        p.add(ALabel,c);
        
        OneIPTextField.setEditable(true);
        c.gridwidth = 1;
        c.gridx = 1;
        c.gridy = 8;
        c.ipadx = 100;
        c.ipady = 1;
        c.weightx = 0.1;
        c.weighty = 0.1;
        p.add(OneIPTextField,c);
        
        JLabel BLabel = new JLabel("Operator #2");
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 9;
        c.ipadx = 100;
        c.ipady = 1;
        c.weightx = 0.1;
        c.weighty = 0.1;
        p.add(BLabel,c);
        
        TwoIPTextField.setEditable(true);
        c.gridwidth = 1;
        c.gridx = 1;
        c.gridy = 9;
        c.ipadx = 100;
        c.ipady = 1;
        c.weightx = 0.1;
        c.weighty = 0.1;
        p.add(TwoIPTextField,c);
        
        JLabel CLabel = new JLabel("Operator #3");
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 10;
        c.ipadx = 100;
        c.ipady = 1;
        c.weightx = 0.1;
        c.weighty = 0.1;
        p.add(CLabel,c);
        
        ThreeIPTextField.setEditable(true);
        c.gridwidth = 1;
        c.gridx = 1;
        c.gridy = 10;
        c.ipadx = 100;
        c.ipady = 1;
        c.weightx = 0.1;
        c.weighty = 0.1;
        p.add(ThreeIPTextField,c);
        
        JButton UpdateIPButton = new JButton("Update IP's");
        UpdateIPButton.setActionCommand("UPDATEIP_PRESS");
        UpdateIPButton.addActionListener(this);
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 11;
        c.ipadx = 100;
        c.ipady = 1;
        c.weightx = 0.1;
        c.weighty = 0.1;
        p.add(UpdateIPButton, c);
        
        return p;
    }
    
    //----------------------------------------------------------------------------------------
    public void mousePressed(MouseEvent e)
    {
    }
     
    //----------------------------------------------------------------------------------------
    public void mouseReleased(MouseEvent e)
    {
    }
     
    //----------------------------------------------------------------------------------------
    public void mouseEntered(MouseEvent e)
    {
    }
     
    //----------------------------------------------------------------------------------------
    public void mouseExited(MouseEvent e)
    {
    }
     
     //----------------------------------------------------------------------------------------
    public void mouseClicked(MouseEvent e)
    {
        submarine.requestFocusInWindow();
    }

    // @Override
    // public synchronized void keyPressed(KeyEvent e)
    // {
        // pressed.add(e.getKeyChar());
        // if (pressed.size() > 1)
        // {
            // Iterator iter = pressed.iterator();
            // while (iter.hasNext())
            // {
                // System.out.println(iter.next() + " pressed");
            // }
        // }
        // System.out.println("==");
    // }

    // @Override
    // public synchronized void keyReleased(KeyEvent e) {
        // pressed.remove(e.getKeyChar());
    // }

    // @Override
    // public void keyTyped(KeyEvent e) {/* Not used */ }
    
    //----------------------------------------------------------------------------------------
    /** Handle the key typed event from the text field. */
    public void keyTyped(KeyEvent e)
    {
        
        if (debug_keyboard) { displayInfo(e, "KEY TYPED: "); }
        if (e.getID() == KeyEvent.KEY_TYPED)
        {
            char c = e.getKeyChar();
            switch (c)
            {
            case 'w':
            case 'd':
            case 's':
            case 'a':
            case 'z':
            case 'x':
            case 'c':
                SendChar(c,robot);
                break;
            }
        }
    }

    //----------------------------------------------------------------------------------------
    /** Handle the key-pressed event from the text field. */
    public void keyPressed(KeyEvent e)
    {
        if (debug_keyboard) { displayInfo(e, "KEY PRESSED: "); }
    }

    //----------------------------------------------------------------------------------------
    /** Handle the key-released event from the text field. */
    public void keyReleased(KeyEvent e)
    {
        if (debug_keyboard) { displayInfo(e, "KEY RELEASED: "); }
    }
    
    //----------------------------------------------------------------------------------------
    private void displayInfo(KeyEvent e, String keyStatus)
    {
        
        //You should only rely on the key char if the event
        //is a key typed event.
        int id = e.getID();
        String keyString;
        if (id == KeyEvent.KEY_TYPED)
        {
            char c = e.getKeyChar();
            keyString = "key character = '" + c + "'";
        }
        else
        {
            int keyCode = e.getKeyCode();
            keyString = "key code = " + keyCode
                    + " ("
                    + KeyEvent.getKeyText(keyCode)
                    + ")";
        }
        
        int modifiersEx = e.getModifiersEx();
        String modString = "extended modifiers = " + modifiersEx;
        String tmpString = KeyEvent.getModifiersExText(modifiersEx);
        if (tmpString.length() > 0)
        {
            modString += " (" + tmpString + ")";
        }
        else
        {
            modString += " (no extended modifiers)";
        }
        
        String actionString = "action key? ";
        if (e.isActionKey())
        {
            actionString += "YES";
        }
        else
        {
            actionString += "NO";
        }
        
        String locationString = "key location: ";
        int location = e.getKeyLocation();
        if (location == KeyEvent.KEY_LOCATION_STANDARD) {
            locationString += "standard";
        } else if (location == KeyEvent.KEY_LOCATION_LEFT) {
            locationString += "left";
        } else if (location == KeyEvent.KEY_LOCATION_RIGHT) {
            locationString += "right";
        } else if (location == KeyEvent.KEY_LOCATION_NUMPAD) {
            locationString += "numpad";
        } else { // (location == KeyEvent.KEY_LOCATION_UNKNOWN)
            locationString += "unknown";
        }
        
        System.out.println("== Key pressed!");
        System.out.println("keyString: " + keyString);
        System.out.println("modifiers: " + modString);
        System.out.println("actionString: " + actionString);
        System.out.println("locationString: " + locationString);
    }
    
    //----------------------------------------------------------------------------------------
    @Override
  	public void actionPerformed(ActionEvent e)
    {
        if ("GENERATEMAZE_PRESS".equals(e.getActionCommand()))
        {
            submarine.GenerateMaze();
        }
		
		if ("STARTTIMER_PRESS".equals(e.getActionCommand()))
        {
            //add action here
        }
		
        
        if ("MASTER_PRESS".equals(e.getActionCommand()))
        {
            MasterButton.setEnabled(false);
            MirrorButton.setEnabled(true);
            GenerateMazeButton.setEnabled(true);
            master = true;
            submarine.SetMaster(true);
        }
        
        if ("MIRROR_PRESS".equals(e.getActionCommand()))
        {
            MasterButton.setEnabled(true);
            MirrorButton.setEnabled(false);
            GenerateMazeButton.setEnabled(false);
            master = false;
            submarine.SetMaster(false);
        }
        
        if ("UPDATEIP_PRESS".equals(e.getActionCommand()))
        {
            JTextField textField = RobotIPTextField;
            String ipPort = null;
            String ip = null;
            String tmpStr = null;
            int port = 55555;
            
            tmpStr = RobotNameTextField.getText();
            char[] tmpRobot = tmpStr.toCharArray();
            switch (tmpRobot[0])
            {
            case 'A':
                robot = 'A';
                break;
            case 'B':
                robot = 'B';
                break;
            case 'C':
                robot = 'C';
                break;
            case 'D':
                robot = 'D';
                break;
            }
            
            submarine.robot = robot;
            networkData.SetLocalPort( Integer.parseInt(LocalPortTextField.getText()) );
            System.out.println("RC: " + networkData.GetLocalPort());
            broadcastHandle1Rx.interrupt();
            broadcastHandle2Rx.interrupt();
            broadcastHandle3Rx.interrupt();
            broadcastHandle4Rx.interrupt();
            broadcast1Rx.interrupt();
            broadcast2Rx.interrupt();
            broadcast3Rx.interrupt();
            broadcast4Rx.interrupt();
			timerThread.interrupt();
            mapRx.interrupt();
            robotRx.interrupt();
            handleRx.interrupt();
            robotRx.SetRobot(robot);
            handleRx.SetRobot(robot);
            
            for (int x = 0; x < 4; ++x)
            {
                switch (x)
                {
                case 0:
                    textField = RobotIPTextField;
                    break;
                case 1:
                    textField = OneIPTextField;
                    break;
                case 2:
                    textField = TwoIPTextField;
                    break;
                case 3:
                    textField = ThreeIPTextField;
                    break;
                }
                ipPort = textField.getText();
                String[] tmp = ipPort.split(":");
                ip = tmp[0];
                port = Integer.parseInt(tmp[1]);
                
                switch (x)
                {
                case 0:
                    networkData.SetRobotAddress(ip);
                    networkData.SetRobotPort(port);
                    break;
                case 1:
                    networkData.SetOneAddress(ip);
                    networkData.SetOnePort(port);
                    break;
                case 2:
                    networkData.SetTwoAddress(ip);
                    networkData.SetTwoPort(port);
                    break;
                case 3:
                    networkData.SetThreeAddress(ip);
                    networkData.SetThreePort(port);
                    break;
                }
            }
        }
    }
    
    public void SendChar(char data, char robot)
    {
        try
        {
            DatagramSocket clientSocket = new DatagramSocket();
            byte[] sendData = new byte[1];
            sendData[0] = (byte) data;
            System.out.println("SendChar(UDP)->Robot: " + data);
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, networkData.GetRobotAddress(), networkData.GetRobotPort());
            clientSocket.send(sendPacket);
            clientSocket.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
}