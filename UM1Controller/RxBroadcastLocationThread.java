import java.io.*;
import java.net.*;
import java.util.*;
import java.nio.ByteBuffer;

import common.Point;

public class RxBroadcastLocationThread extends Thread{
	private UM1 submarine;
    private NetworkData networkData;
    private int port;
    private char robot;
    private Point pt = new Point(0,0);
    private DatagramSocket dsocket;
    
	public RxBroadcastLocationThread(UM1 submarine, NetworkData networkData, int port, char robot)
    {
        this.port  = port;
		this.submarine = submarine;
        this.robot = robot;
        this.networkData = networkData;
	}
    
	public void run()
    {
        int x = 0;
        int y = 0;
        char rxRobot = 'A';
        while(true)
        {
            try
            {
                dsocket = new DatagramSocket(port);
                
                byte[] buffer = new byte[128];

                // Create a packet to receive data into the buffer
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                while(true)
                {
                    dsocket.receive(packet);

                    // Convert the contents to a string, and display them
                    String stringToProcess = new String(buffer, 0, packet.getLength());
                    // Reset the length of the packet before reusing it.
                    packet.setLength(buffer.length);
                    String[] tmp = stringToProcess.split(",");
                    System.out.println(stringToProcess);
                    tmp[1] = tmp[1].replaceAll( "[^\\d]", "" );
                    Point pt = new Point(Integer.parseInt(tmp[0]),Integer.parseInt(tmp[1]));
                    switch (robot)
                    {
                    case 'A':
                        {
                            synchronized(this.submarine){
								submarine.UpdateA(pt);
								
							}
                        }
                        break;
					case 'B':
                        {
                            synchronized(this.submarine){
								submarine.UpdateB(pt);
								
							}
                        }
                        break;
					case 'C':
                        {
                            synchronized(this.submarine){
								submarine.UpdateC(pt);
								
							}
                        }
                        break;
					case 'D':
                        {
                            synchronized(this.submarine){
								submarine.UpdateD(pt);
								
							}
                        }
                        break;
                    default:
                        break;
                    }
                }
            }
            catch(Exception e)
            {
                //e.printStackTrace();
                //System.exit(-1);
            }
        } // end of the while(true)
    }
    
    public void SetRobot(char robot)
    {
        this.robot = robot;
        // switch (robot)
        // {
            // case 'A':
                // port += 1;
                // break;
            // case 'B':
                // port += 2;
                // break;
            // case 'C':
                // port += 3;
                // break;
            // case 'D':
                // port += 4;
                // break;
        // }
    }
    
   @Override
   public void interrupt()
   {
        super.interrupt();  
        this.dsocket.close();
        this.port = networkData.GetLocalPort();
        switch (robot)
        {
            case 'A':
                port += 1;
                break;
            case 'B':
                port += 2;
                break;
            case 'C':
                port += 3;
                break;
            case 'D':
                port += 4;
                break;
        }
   }
    
  static byte[] readData(ServerSocket serverSocket)
  {
    Socket clientSocket = null;
    InputStream in = null;
    try
    {
      int bufferSize = 150;   
      clientSocket = serverSocket.accept();                          
      in = clientSocket.getInputStream();
      byte[] data = new byte[bufferSize];
      int count = in.read(data, 0, bufferSize);
      Vector v = new Vector();   
      int totalLength = 0;          
      while(count != -1)
      {
        v.addElement(data);
        v.addElement(count);
        totalLength = totalLength + count;
        data = new byte[bufferSize];
        count = in.read(data, 0, bufferSize);            
      }

      // use totalLength to create the overall byte string
      data = new byte[totalLength];
      int currentPosition = 0;
      for(int i = 0; i < v.size(); i = i + 2)
      {
        int currentLength = ((Integer)v.elementAt(i+1)).intValue();
        System.arraycopy((byte[])v.elementAt(i), 0, data, currentPosition, currentLength);
        currentPosition = currentPosition + currentLength;
      }
      in.close();
      clientSocket.close();    
      return data;
    }
    catch(Exception e)
    {
      e.printStackTrace();   
      if(clientSocket != null)
      {
        try {
          clientSocket.close();
        }
        catch(Exception ee)
        {
          ee.printStackTrace();
        }
      }      
    }
    return null;
  }
}

