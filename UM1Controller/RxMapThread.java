import java.io.*;
import java.net.*;
import java.util.*;
import java.nio.ByteBuffer;

import common.Point;

public class RxMapThread extends Thread{
	private UM1 submarine;
    private NetworkData networkData;
    private int port;
    private char robot;
    private Point pt = new Point(0,0);
    private DatagramSocket dsocket;
    
	public RxMapThread(UM1 submarine, NetworkData networkData, int port, char robot)
    {
        this.port  = port;
		this.submarine = submarine;
        this.robot = robot;
        this.networkData = networkData;
	}
    
	public void run()
    {
        int x = 0;
        int y = 0;
        char rxRobot = 'A';
        while(true)
        {
            try
            {
                dsocket = new DatagramSocket(port);
                
                byte[] buffer = new byte[1024];

                // Create a packet to receive data into the buffer
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                while(true)
                {
                    System.out.println("RxMapThread: waiting on " + port);
                    dsocket.receive(packet);
                    
                    // Convert the contents to a string, and display them
                    byte[] data = buffer;
                    // String stringToProcess = new String(buffer, 0, packet.getLength());

                    // // Reset the length of the packet before reusing it.
                    packet.setLength(buffer.length);
                    // byte[] data = stringToProcess.getBytes();

                    synchronized(this.submarine)
                    {
                        submarine.SetMap(data, 900);
                    }
                    
                    // Reset the length of the packet before reusing it.
                    packet.setLength(buffer.length);
                }
            }
            catch(Exception e)
            {
                //e.printStackTrace();
                //System.exit(-1);
            }
        } // end of the while(true)
    }
    
   @Override
   public void interrupt()
   {
        super.interrupt();  
        this.dsocket.close();
        this.port = networkData.GetLocalPort() + 5;
   }
    
  static byte[] readData(ServerSocket serverSocket)
  {
    Socket clientSocket = null;
    InputStream in = null;
    try
    {
      int bufferSize = 150;   
      clientSocket = serverSocket.accept();                          
      in = clientSocket.getInputStream();
      byte[] data = new byte[bufferSize];
      int count = in.read(data, 0, bufferSize);
      Vector v = new Vector();   
      int totalLength = 0;          
      while(count != -1)
      {
        v.addElement(data);
        v.addElement(count);
        totalLength = totalLength + count;
        data = new byte[bufferSize];
        count = in.read(data, 0, bufferSize);            
      }

      // use totalLength to create the overall byte string
      data = new byte[totalLength];
      int currentPosition = 0;
      for(int i = 0; i < v.size(); i = i + 2)
      {
        int currentLength = ((Integer)v.elementAt(i+1)).intValue();
        System.arraycopy((byte[])v.elementAt(i), 0, data, currentPosition, currentLength);
        currentPosition = currentPosition + currentLength;
      }
      in.close();
      clientSocket.close();    
      return data;
    }
    catch(Exception e)
    {
      e.printStackTrace();   
      if(clientSocket != null)
      {
        try {
          clientSocket.close();
        }
        catch(Exception ee)
        {
          ee.printStackTrace();
        }
      }      
    }
    return null;
  }
}

