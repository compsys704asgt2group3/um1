import java.io.*;
import java.net.*;
import java.util.*;
import java.nio.ByteBuffer;
import java.math.BigInteger;

import common.Point;

public class TimerThread extends Thread{
	private UM1 submarine;
    private NetworkData networkData;
    private int port;
    private char robot;
    private Point pt = new Point(0,0);
    private DatagramSocket dsocket2;
	private BigInteger timeLeft;
    
	public TimerThread(UM1 submarine, NetworkData networkData, int port)
    {
        this.port  = port;
		this.submarine = submarine;
        //this.robot = robot;
        this.networkData = networkData;
		
	}
    
	public void run()
    {
        int x = 0;
        int y = 0;
        while(true)
        {
            try
            {
                dsocket2 = new DatagramSocket(port);
                
                byte[] buffer = new byte[32];

                // Create a packet to receive data into the buffer
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                while(true)
                {	
					//System.out.println("timerThread: waiting on " + port);
					
                    dsocket2.receive(packet);

                    // Convert the contents to a string, and display them
                    String stringToProcess = new String(buffer, 0, packet.getLength());
                    // Reset the length of the packet before reusing it.
                    packet.setLength(buffer.length);
                    //String[] tmp = stringToProcess.split(",");
                    //System.out.println("String to process is: " + stringToProcess + "and then some");
					
					//System.out.println(stringToProcess.length() + "is length");
					
					// Remove null characters from byte string
					for (int i = 0; i < stringToProcess.length(); i++) {
						if (stringToProcess.charAt(i) == '\u0000') {
							stringToProcess = stringToProcess.substring(0, i);
							break;
						}
					}
					//System.out.println(stringToProcess.length() + "is length");
					
					int clock = Integer.parseInt(stringToProcess);
					int colour = 1;
					
					if (clock > 480000) {
						colour = 1;
					} else if ( clock > 300000) {
						colour = 2;
					} else if (clock > 150000) {
						colour = 3;
					} else if (clock > 60000) {
						colour = 4;
					} else {
						colour = 5;
					}
					//System.out.println(clock);
					int minutes;
					int seconds;
					int milliseconds;
					int missionStatus = 1;
					//if (clock % 1000 == 0) {
					minutes = (clock/(1000*60)) % 60;
					seconds = (clock/1000) % 60;
					milliseconds = clock % 1000 / 100;
											

					String timeLeft = new String("" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds) + "." + String.format("%01d", milliseconds));
					
					if (clock == 0) {missionStatus = 0;}
					//}
					
					
                     synchronized(this.submarine)
                    { 
                        submarine.UpdateTime(timeLeft, colour, missionStatus);
                    }
                    //stringToProcess.replaceAll( "[^\\d]", "" );
                    //BroadcastLocation(stringToProcess);
                }
            }
            catch(Exception e)
            {
                //e.printStackTrace();
                //System.exit(-1);
            }
        } // end of the while(true)
    }
	
   @Override
   public void interrupt()
   {
        super.interrupt();  
        this.dsocket2.close();
        this.port = networkData.GetLocalPort() + 7;
   }
    
    // public void BroadcastLocation(String data)
    // {
        // String data2 = data;
        // try
        // {
            // data2.concat("," + robot);
            // DatagramSocket clientSocket = new DatagramSocket();
            // byte[] dataToSend = data2.getBytes();
            // System.out.println("SendChar(UDP)->Robot: " + data2);
            // DatagramPacket sendPacket = new DatagramPacket(dataToSend, dataToSend.length, networkData.GetOneAddress(), networkData.GetOnePort());
            // clientSocket.send(sendPacket);
            // sendPacket = new DatagramPacket(dataToSend, dataToSend.length, networkData.GetTwoAddress(), networkData.GetTwoPort());
            // clientSocket.send(sendPacket);
            // sendPacket = new DatagramPacket(dataToSend, dataToSend.length, networkData.GetThreeAddress(), networkData.GetThreePort());
            // clientSocket.send(sendPacket);
            // clientSocket.close();
        // }
        // catch (Exception e)
        // {
            // //e.printStackTrace();
        // }
    // }
    
   // @Override
   // public void interrupt()
   // {
        // super.interrupt();  
        // this.dsocket.close();
        // this.port = networkData.GetLocalPort();
   // }
    
  // static byte[] readData(ServerSocket serverSocket)
  // {
    // Socket clientSocket = null;
    // InputStream in = null;
    // try
    // {
      // int bufferSize = 150;   
      // clientSocket = serverSocket.accept();                          
      // in = clientSocket.getInputStream();
      // byte[] data = new byte[bufferSize];
      // int count = in.read(data, 0, bufferSize);
      // Vector v = new Vector();   
      // int totalLength = 0;          
      // while(count != -1)
      // {
        // v.addElement(data);
        // v.addElement(count);
        // totalLength = totalLength + count;
        // data = new byte[bufferSize];
        // count = in.read(data, 0, bufferSize);            
      // }

      // // use totalLength to create the overall byte string
      // data = new byte[totalLength];
      // int currentPosition = 0;
      // for(int i = 0; i < v.size(); i = i + 2)
      // {
        // int currentLength = ((Integer)v.elementAt(i+1)).intValue();
        // System.arraycopy((byte[])v.elementAt(i), 0, data, currentPosition, currentLength);
        // currentPosition = currentPosition + currentLength;
      // }
      // in.close();
      // clientSocket.close();    
      // return data;
    // }
    // catch(Exception e)
    // {
      // e.printStackTrace();   
      // if(clientSocket != null)
      // {
        // try {
          // clientSocket.close();
        // }
        // catch(Exception ee)
        // {
          // ee.printStackTrace();
        // }
      // }      
    // }
    // return null;
  // }
}

